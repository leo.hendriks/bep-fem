import numpy as np
from Matrices import element_area

def assemble_vector(elems, coor, n_nodes, area, t, source_function):
    """
    Assembles the total vector from the element vectors

    :param elems: tags of the element nodes
    :param coor: The coordinates of the nodes
    :param n_nodes: the total number of nodes
    :param element_function: the function to calculate the element vectors
    :param t: time to evaluate the source_function at
    :param source_function: source function of the FEM problem
    :return: The assembled vector
    """

    f = np.zeros(n_nodes)
    i = elems.ravel()

    data = np.abs(area)[:, np.newaxis] / 6. * source_function(coor[elems, 0], coor[elems, 1], t)
    np.add.at(f, i, data.ravel())
    return f


def assemble_vector_old(elems, coor, n_nodes, element_function, t, source_function):
    """
    Assembles the total vector from the element vectors

    :param elems: tags of the element nodes
    :param coor: The coordinates of the nodes
    :param n_nodes: the total number of nodes
    :param element_function: the function to calculate the element vectors
    :param t: time to evaluate the source_function at
    :param source_function: source function of the FEM problem
    :return: The assembled vector
    """

    f = np.zeros(n_nodes)
    for k, elem in enumerate(elems):
        x = coor[elem, 0]
        y = coor[elem, 1]
        f_ek = element_function(x, y, t, source_function)
        for i in range(3):
            # TODO: Is improving this possible?
            f[elems[k, i]] += f_ek[i]
    return f


def generate_element_vector(x, y, t, source_function):
    return np.abs(element_area(x, y)/2.)/3.*source_function(x, y, t)



