import numpy as np
import gmsh


# noinspection DuplicatedCode
def init_mesh(vertices_x, vertices_y, boundary_dir, lc):
    """
    This function creates a mesh based on the given maximum element size(lc).

    :param vertices_x: the x coordinates of the vertices of the mesh
    :param vertices_y: the y coordinates of the vertices of the mesh
    :param boundary_dir: the type of boundary's encountered when walking around the vertices
    :param lc: the maximum element size
    :return:    the number of nodes,
                the x location of the nodes,
                the y location of the nodes,
                the node tags of th elements,
                the nodes on the boundary
    """
    # Mesh generation
    model = gmsh.model
    geometry = model.geo

    # GMSH Initialization
    gmsh.initialize()
    gmsh.option.setNumber("General.Terminal", 0)
    model.add("Shape")

    # Creating the points of our mesh based on the vertices given to the fem class
    points = []
    for i, (x, y) in enumerate(zip(vertices_x, vertices_y)):
        points.append(geometry.addPoint(x, y, 0, lc))
    # Creating four lines between the points of our Mesh
    lines = []
    for i, j in zip(points, points[1:] + points[:1]):
        lines.append(geometry.addLine(i, j))
    # Creating a curve loop over the four lines and then adding a plane to the curve loop
    curves = [geometry.addCurveLoop(lines)]
    geometry.addPlaneSurface(curves)

    # Generating the 2d mesh and writing it to the file mesh.msh
    geometry.synchronize()
    model.mesh.generate(2)

    # Getting the location of the nodes and node tags of the elements.
    coor = model.mesh.getNodes()[1]
    el_node_tag = model.mesh.getElements(2, -1)[2]

    # Reshape the coor array and remove the z coordinate
    coor = np.delete(np.array(coor).reshape(((len(coor) // 3), 3)), 2, 1)

    # Calculating the total number of nodes
    n_nodes = coor.shape[0]

    # Getting an array with node tags of each element on the columns
    elems = np.array(el_node_tag[0]).reshape(((len(el_node_tag[0]) // 3), 3)) - 1

    # Dividing the boundary into Direchlet and Neumann boundary nodes
    i_bnd_dir = []
    i_bnd_neu = []
    for i, bound_type in enumerate(boundary_dir):
        if bound_type == 1:
            i_bnd_dir.append(model.mesh.getNodes(1, lines[i], True, False)[0] - 1)
        elif bound_type == 0:
            i_bnd_neu.append(model.mesh.getNodes(1, lines[i], True, False)[0] - 1)

    # Closing the gmsh API
    gmsh.finalize()

    return n_nodes, coor, elems, i_bnd_dir, i_bnd_neu
