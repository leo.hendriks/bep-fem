import numpy
from scipy import sparse
import numpy as np


def assemble_matrix_old(elems, coor, n_nodes, element_function):
    """
    Assembles the total matrix from the element matrices

    :param elems: tags of the element nodes
    :param coor: The coordinates of the nodes
    :param n_nodes: the total number of nodes
    :param element_function: the function to calculate the element matrices
    :return: The assembled matrix
    """

    a = sparse.lil_matrix((n_nodes, n_nodes))
    for k, elem in enumerate(elems):
        x = coor[elem, 0]
        y = coor[elem, 1]
        s_ek = element_function(x, y)
        for i in range(3):
            for j in range(3):
                # TODO: Is improving this possible?
                a[elem[i], elem[j]] += s_ek[i, j]
    return a


def assemble_stiffness_matrix(elems, coor, n_nodes, area):
    """
    Assembles the stiffness matrix from the mesh data

    :param elems: tags of the element nodes
    :param coor: The coordinates of the nodes
    :param n_nodes: the total number of nodes
    :param area: The area of each element
    :return: The assembled matrix
    """
    i, j = get_sparse_index(elems)

    data = generate_stiffness_matrix_data(coor, elems, area)
    return sparse.csc_matrix((data, (i, j)), shape=(n_nodes, n_nodes))


def generate_stiffness_matrix_data(coor, elems, area):
    """
    This function calculates the data for the stiffness matrix.
    See the implementation section of the report for an explanation of the calculation method.

    :param coor: The coordinates of the nodes
    :param elems: The tags of the element nodes
    :param area: The area of each element
    :return: data for the stiffness matrix
    """
    a = np.sum((coor[elems[:, 1]] - coor[elems[:, 2]]) ** 2, axis=1) / area / 2.
    b = np.sum((coor[elems[:, 2]] - coor[elems[:, 0]]) ** 2, axis=1) / area / 2.
    c = np.sum((coor[elems[:, 1]] - coor[elems[:, 2]]) * (coor[elems[:, 2]] - coor[elems[:, 0]]), axis=1) / area / 2.

    return numpy.concatenate((a, c, - a - c, c, b, - b - c, - a - c, - b - c, a + b + 2 * c), axis=None)


def generate_element_stiffness_matrix(x, y):
    """
    This function calculates the element stiffness matrix for given x and y coordinates of the element.
    See the implementation section of the report for an explanation of the calculation method.

    :param x: x coordinates of the nodes
    :param y: y coordinates of the nodes
    :return: element matrix Sei
    """
    return 1 / 2. / element_area(x, y) * np.array([
        [(y[1] - y[2]) ** 2 + (x[2] - x[1]) ** 2,
         (y[1] - y[2]) * (y[2] - y[0]) + (x[2] - x[1]) * (x[0] - x[2]),
         (y[1] - y[2]) * (y[0] - y[1]) + (x[2] - x[1]) * (x[1] - x[0])],
        [(y[1] - y[2]) * (y[2] - y[0]) + (x[2] - x[1]) * (x[0] - x[2]),
         (y[2] - y[0]) ** 2 + (x[0] - x[2]) ** 2,
         (y[2] - y[0]) * (y[0] - y[1]) + (x[0] - x[2]) * (x[1] - x[0])],
        [(y[1] - y[2]) * (y[0] - y[1]) + (x[2] - x[1]) * (x[1] - x[0]),
         (y[2] - y[0]) * (y[0] - y[1]) + (x[0] - x[2]) * (x[1] - x[0]),
         (y[0] - y[1]) ** 2 + (x[1] - x[0]) ** 2]
    ])


def assemble_mass_matrix(elems, n_nodes, area):
    """
    Assembles the mass matrix from the mesh data

    :param elems: tags of the element nodes
    :param n_nodes: the total number of nodes
    :param area: The area of each element
    :return: The assembled matrix
    """
    i, j = get_sparse_index(elems)

    data = generate_mass_matrix_data(area)
    return sparse.csc_matrix((data, (i, j)), shape=(n_nodes, n_nodes))


def generate_mass_matrix_data(area):
    """
    This function calculates the data for the mass matrix.
    See the implementation section of the report for an explanation of the calculation method.

    :param area: The area of each element
    :return: data for the mass matrix
    """
    data = np.abs(area) / 24.
    m_ek = np.array([[2.0, 1.0, 1.0], [1.0, 2.0, 1.0], [1.0, 1.0, 2.0]]).ravel()

    return np.kron(m_ek, data)


def generate_element_mass_matrix(x, y):
    """
    This function calculates the element mass matrix for given x and y coordinates of the element.
    See the implementation section of the report for an explanation of the calculation method.

    :param x: x coordinates of the nodes
    :param y: y coordinates of the nodes
    :return: element mass matrix Mei
    """
    # TODO: Double check computation in report
    return np.abs(element_area(x, y)) / 24 * np.array([[2.0, 1.0, 1.0], [1.0, 2.0, 1.0], [1.0, 1.0, 2.0]])


def element_area(x, y):
    return x[0] * y[1] - x[0] * y[2] - x[1] * y[0] + x[1] * y[2] + x[2] * y[0] - x[2] * y[1]


def generate_nonlin_matrix_constants(elems, area):
    """
    Assembles the values that remain constant for the N matrix computation

    :param elems: tags of the element nodes
    :param area: The area of each element
    :return: The two index arrays and the data array that needs to be multiplied by the solution
    """

    i, j = get_sparse_index(elems)

    i = np.kron(i, [1, 1, 1])
    j = np.kron(j, [1, 1, 1])

    data = np.abs(area) / 120.

    n_ek = np.array(
        [[[6., 2., 2.],
          [2., 2., 1.],
          [2., 1., 2.]],
         [[2., 2., 1.],
          [2., 6., 2.],
          [1., 2., 2.]],
         [[2., 1., 2.],
          [1., 2., 2.],
          [2., 2., 6.]]]).reshape((9, 3))

    data = data[:, np.newaxis] @ n_ek[:, np.newaxis, :]

    return i, j, data


def assemble_nonlin_matrix(i, j, data, elems, n_nodes, cl):
    """
    Assembles the sparse N matrix from the constants and the current solution

    :param i: The 1st index for the sparse matrix data
    :param j: The 2nd index for the sparse matrix data
    :param data: The data for the sparse matrix that needs to still be multiplied by the solution
    :param elems: tags of the element nodes
    :param n_nodes: the total number of nodes
    :param cl: the current solution vector
    :return: The sparse N matrix
    """
    n_data = cl[elems] * data
    return sparse.csc_matrix((n_data.ravel(), (i, j)), shape=(n_nodes, n_nodes))


def assemble_nonlin_matrix_old(elems, coor, n_nodes, cl):
    """
    Assembles the total N matrix from the element matrices

    :param elems: tags of the element nodes
    :param coor: The coordinates of the nodes
    :param n_nodes: the total number of nodes
    :param cl: the current solution vector
    :return: The assembled  N matrix
    """

    a = sparse.lil_matrix((n_nodes, n_nodes))
    n_ek = 1. / 120. * np.array(
        [[[6., 2., 2.],
          [2., 2., 1.],
          [2., 1., 2.]],
         [[2., 2., 1.],
          [2., 6., 2.],
          [1., 2., 2.]],
         [[2., 1., 2.],
          [1., 2., 2.],
          [2., 2., 6.]]])
    for k, elem in enumerate(elems):
        x = coor[elem, 0]
        y = coor[elem, 1]
        delta = np.abs(element_area(x, y))
        for i in range(3):
            for j in range(3):
                # TODO: Is improving this possible?
                a[elem[i], elem[j]] += delta * (cl[elem[0]] * n_ek[0, i, j] +
                                                cl[elem[1]] * n_ek[1, i, j] +
                                                cl[elem[2]] * n_ek[2, i, j])
    return a


def calculate_area(coor, elems):
    c1 = coor[elems[:, 0], :]
    d21 = coor[elems[:, 1], :] - c1
    d31 = coor[elems[:, 2], :] - c1
    return np.cross(d21, d31)


def get_sparse_index(elems):
    ind_i = []
    ind_j = []
    for ind1 in range(3):
        for ind2 in range(3):
            ind_i.append(ind1)
            ind_j.append(ind2)

    i = elems[:, ind_i].ravel("F")
    j = elems[:, ind_j].ravel("F")

    return i, j
