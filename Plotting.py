import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib.tri import Triangulation
from matplotlib import cm

import numpy as np


def mesh_plot(coor, elems, i_bnd_dir, i_bnd_neu):
    """
    This function creates a plot of the mesh

    :param coor: The coordinates of the nodes
    :param elems: tags of the element nodes
    :param i_bnd_dir: tags of the nodes with dir boundary
    :param i_bnd_neu: tags of the nodes with neu boundary
    :return: figure of the plotted mesh
    """
    fig = plt.figure()
    c = np.full(coor.shape[0], 'k')
    for bnd in i_bnd_neu:
        c[bnd] = 'r'
    for bnd in i_bnd_dir:
        c[bnd] = 'b'


    # TODO: Rewrite this implementation to not need a for loop
    for ele in elems:
        ele_plus = np.append(ele, ele[0])
        plt.plot(coor[ele_plus,0], coor[ele_plus,1], 'slategray')

    plt.scatter(coor[:, 0], coor[:, 1], c=c)

    plt.xlabel("x (m)")
    plt.ylabel("y (m)")

    return fig


def matrix_plot(a):
    """
    Function used for plotting matrices
    :param a: The matrix to be plotted
    :return: the figure of the plot
    """
    print(a)
    fig = plt.matshow(a.toarray())

    plt.colorbar()

    return fig

def triangle_plot_function(coor, elems, t, func, title):
    """
        This function creates an animation of a triangle plot for the given mesh, time values and function

        :param coor: The coordinates of the nodes
        :param elems: The nodes contained in each element
        :param t: The time values to plot in the animation
        :param func: The function to be plotted
        :return:    The figure of the plot,
                    The axes of the plot,
                    The animation for the plot

        """
    u = np.empty((t.size, coor.shape[0]))
    for i, t_value in enumerate(t):
        u[i, :] = func(coor[:, 0], coor[:, 1], t_value)

    return triangle_plot(coor, elems, u, title, t=t)


def triangle_plot(coor, elems, u, title, t=None):
    """
            This function creates an animation of a triangle plot for the given mesh and values

            :param coor: the coordinates of the nodes
            :param elems: The nodes contained in each element
            :param u: values to plot
            :return:    The figure of the plot,
                        The axes of the plot,
                        The animation for the plot

            """
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    trian = Triangulation(coor[:, 0], coor[:, 1], triangles=elems)

    plot = [ax.plot_trisurf(trian, u[0, :], cmap=cm.coolwarm)]
    if t is not None:
        ax.set_title(title + f' (t={t[0]:.2e})')
    else:
        ax.set_title(title)

    # Set limits and initival viewpoint
    ax.set_zlim3d(np.min(u), np.max(u))
    ax.view_init(0, -90)

    # Animation of graph
    def update_graph(num):
        plot[0].remove()
        plot[0] = ax.plot_trisurf(trian, u[num, :], cmap=cm.coolwarm)
        if t is not None:
            ax.set_title(title + f'(t={t[num]:.2e})')
    ani = FuncAnimation(fig, update_graph, u.shape[0], blit=False, interval=200)

    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")

    return fig, ax, ani

def triangle_sub_plot(coor, elems, u, title, t, t_ind):
    """
            This function creates an animation of a triangle plot for the given mesh and values

            :param coor: the coordinates of the nodes
            :param elems: The nodes contained in each element
            :param u: values to plot
            :return:    The figure of the plot,
                        The axes of the plot,
                        The animation for the plot

            """
    fig = plt.figure()
    fig.subplots_adjust(wspace=0, hspace=0)
    fig.suptitle(title) #, fontsize=14)
    for i, ind in enumerate(t_ind):
        ax = fig.add_subplot(2, 2, i+1, projection='3d')
        trian = Triangulation(coor[:, 0], coor[:, 1], triangles=elems)

        plot = [ax.plot_trisurf(trian, u[ind, :], cmap=cm.coolwarm)]

        ax.set_title(f't={t[ind]:.3e}')

        # Set limits and initival viewpoint
        ax.set_zlim3d(np.min(u), np.max(u))
        ax.view_init(0, -90)

        ax.set_xlabel("x")
        ax.set_ylabel("y")
        ax.set_zlabel("z")

    return fig, ax

def plot_radial1d(coor, u, title, t=None, ana_func=None):
    fig, ax = plt.subplots()

    r = np.sqrt(coor[:, 0] ** 2 + coor[:, 1] ** 2)

    line, = ax.plot(r, u[0, :], '.')

    if t is not None:
        ax.set_title(title + f' (t={t[0]:.2e})')
    else:
        ax.set_title(title)

    if ana_func is not None:
        r_range = np.linspace(np.min(r), np.max(r), 1000)
        ana = ana_func(r_range, t)
        ana_line, = ax.plot(r_range, ana[0, :])

    plt.ylim((np.min(u), np.max(u)))

    def update_graph(num):
        line.set_ydata(u[num, :])
        if t is not None:
            ax.set_title(title + f'(t={t[num]:.2e})')
        if ana_func is not None:
            ana_line.set_ydata(ana[num, :])

        return line, ana_line,

    ani = FuncAnimation(fig, update_graph, u.shape[0], interval=200, blit=False, save_count=50)

    ax.set_xlabel("r")
    ax.set_ylabel("z")

    return fig, ax, ani

def sub_plot_radial1d(coor, u, title, t, t_ind, ana_func=None):
    r = np.sqrt(coor[:, 0] ** 2 + coor[:, 1] ** 2)
    indices = r.argsort()

    r = r[indices]
    u = u[:, indices]

    r_min = np.min(r)
    r_max = np.max(r)

    if ana_func is not None:
        r_range = np.linspace(r_min, r_max, 1000)
        ana = ana_func(r_range, t)

        y_min = min(np.min(u), np.min(ana))
        y_max = max(np.max(u), np.max(ana))
    else:
        y_min = np.min(u)
        y_max = np.max(u)


    fig = plt.figure()
    # fig.subplots_adjust(wspace=0, hspace=0)
    fig.suptitle(title)
    for i, ind in enumerate(t_ind):
        ax = fig.add_subplot(2, 2, i + 1)

        if ana_func is not None:
            ana_line, = ax.plot(r_range, ana[ind, :])
            plt.ylim((y_min, y_max))

        else:
            plt.ylim((np.min(u), np.max(u)))

        line, = ax.plot(r, u[ind, :])

        ax.set_title(f't={t[ind]:.2e}')

        plt.xlim((r_min, r_max))

        if ana_func is not None:
            plt.legend(("analytical solution", "FEM solution"))

        plt.ticklabel_format(axis="both", style="sci", scilimits=(-2, 2))


        ax.set_xlabel("r (m)")
        ax.set_ylabel("p (Pa)")

    return fig, ax

def plot_1d_x(coor, u, title, t=None, ana_func=None):
    fig, ax = plt.subplots()

    x = coor[:, 0]

    indices = x.argsort()

    x = x[indices]
    u = u[:, indices]

    line, = ax.plot(x, u[0, :])

    if t is not None:
        ax.set_title(title + f' (t={t[0]:.2e})')
    else:
        ax.set_title(title)

    if ana_func is not None:
        x_range = np.linspace(np.min(x), np.max(x), 1000)
        X, T = np.meshgrid(x_range, t)
        ana = ana_func(X, T)
        ana_line, = ax.plot(x_range, ana[0, :])

    plt.ylim((np.min(u), np.max(u)))

    def update_graph(num):
        line.set_ydata(u[num, :])
        if t is not None:
            ax.set_title(title + f'(t={t[num]:.2e})')
        if ana_func is not None:
            ana_line.set_ydata(ana[num, :])
            return line, ana_line,
            plt.legend(("FEM solution", "analytical solution"))

        return line,

    ani = FuncAnimation(fig, update_graph, u.shape[0], interval=200, blit=False, save_count=50)

    ax.set_xlabel("x (m)")
    ax.set_ylabel("p (Pa)")

    return fig, ax, ani

def sub_plot_1d_x(coor, u, title, t, t_ind, ana_func=None):
    x = coor[:, 0]
    indices = x.argsort()

    x = x[indices]
    u = u[:, indices]

    x_min = np.min(x)
    x_max = np.max(x)

    if ana_func is not None:
        x_range = np.linspace(x_min, x_max, 1000)
        X, T = np.meshgrid(x_range, t)
        ana = ana_func(X, T)

        y_min = min(np.min(u), np.min(ana))
        y_max = max(np.max(u), np.max(ana))
    else:
        y_min = np.min(u)
        y_max = np.max(u)


    fig = plt.figure()
    # fig.subplots_adjust(wspace=0, hspace=0)
    fig.suptitle(title)
    for i, ind in enumerate(t_ind):
        ax = fig.add_subplot(2, 2, i + 1)



        if ana_func is not None:
            ana_line, = ax.plot(x_range, ana[ind, :])
            plt.ylim((y_min, y_max))
        else:
            plt.ylim((np.min(u), np.max(u)))

        line, = ax.plot(x, u[ind, :])

        ax.set_yscale('log')

        # ax.set_title(f't={t[ind]:.2e}')
        ax.set_title(f'iteration {t[ind]:d}')

        # plt.xlim((x_min, x_max))

        if ana_func is not None:
            plt.legend(("analytical solution", "FEM solution"))

        # plt.ticklabel_format(axis="both", style="sci", scilimits=(-2, 2))

        # ax.set_xlabel("x (m)")
        ax.set_xlabel("x")
        # ax.set_ylabel("p (Pa)")
        ax.set_ylabel("y")

    return fig, ax

def plot_1d_x_zoom(coor, u, title, t, t_index, xmin, xmax, ana_func=None):
    x = coor[:, 0]
    indices = x.argsort()

    x = x[indices]
    u = u[:, indices]

    x_min = xmin
    x_max = xmax

    if ana_func is not None:
        x_range = np.linspace(x_min, x_max, 1000)
        X, T = np.meshgrid(x_range, t)
        ana = ana_func(X, T)

        y_min = min(np.min(u), np.min(ana))
        y_max = max(np.max(u), np.max(ana))
    else:
        y_min = np.min(u)
        y_max = np.max(u)


    fig = plt.figure()
    # fig.subplots_adjust(wspace=0, hspace=0)
    ax = fig.add_subplot(1, 1, 1)

    line, = ax.plot(x, u[t_index, :])

    if ana_func is not None:
        ana_line, = ax.plot(x_range, ana[t_index, :])
        plt.ylim((y_min, y_max))
        plt.legend(("FEM solution", "analytical solution"))
    else:
        plt.ylim((np.min(u), np.max(u)))

    ax.set_title(title + f' t={t[t_index]:.2e}')

    plt.xlim((x_min, x_max))



    plt.ticklabel_format(axis="both", style="sci", scilimits=(-2, 2))

    ax.set_xlabel("x (m)")
    ax.set_ylabel("p (Pa)")

    return fig, ax
