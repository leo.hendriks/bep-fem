import time
import matplotlib.pyplot as plt


def func_timer_maker(func_name):
    def func_timer(function):
        def wrapper(*args, **kwargs):
            start = time.time()
            print("Starting " + func_name + "...")
            _ = function(*args, **kwargs)
            print(func_name + " took %0.5f s" % (time.time() - start))
            plt.show()
            return _
        return wrapper
    return func_timer
