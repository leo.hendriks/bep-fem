from FEM import FEM
import numpy as np

Lx = 1
Ly = Lx / 10

# Parameters
fem = FEM([0, Lx, Lx, 0], [0, 0, Ly, Ly], [0, 1, 0, 1], [3/2., 6.])

fem.lc = 0.002
fem.damping = 0.3
fem.total_iters = 50
fem.alpha = -1
fem.t = range(fem.total_iters)

fem.sol_func = lambda x, y, t: 6 / (1 + x) ** 2

# fem.plot_sol_func()

# fem.solve_non_lin_diff()
# fem.save_solution()

fem.load_solution()

# fem.plot_mesh()
# fem.plot_solution()
fem.sub_plot_solution_1d_x("Non-linear Diffusion Solution", [0, 2, 4, 20], lambda x, t: fem.sol_func(x, 0, t))

fem.n_nodes = fem.coor.shape[0]
fem.t = np.arange(fem.u.shape[0])
fem.calc_error_with_ana()

print(fem.diff.shape)
bnd = np.unique(np.where(fem.diff == 0)[1])
fem.u = np.delete(fem.u, bnd, 1)
fem.diff = np.delete(fem.diff, bnd, 1)
fem.coor = np.delete(fem.coor, bnd, 0)
fem.sub_plot_error_1d_x("Non-linear Diffusion Error", [0, 2, 4, 20])
