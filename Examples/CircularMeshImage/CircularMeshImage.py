from FEM import FEM
import numpy as np
import matplotlib.pyplot as plt

# Parameters
c0 = 1500.
r0 = 1000.
beta = 10.
t_end = 1e-3
sig = 0.1
p0 = 1e6

# Parameters
Radius = 1.5 * c0 * t_end
print(f"R equals: {Radius}")

# FEM
fem = FEM([Radius, Radius / 2, -Radius / 2, -Radius, -Radius / 2, Radius / 2],
          [0, np.sqrt(3) * Radius / 2, np.sqrt(3) * Radius / 2, 0, -np.sqrt(3) * Radius / 2, -np.sqrt(3) * Radius / 2])
fem.beta = beta
print(f"Beta is equal to: {fem.beta}")
fem.r0 = r0
fem.c0 = c0

# Time interval
fem.t_begin = 0
fem.t_end = t_end
fem.t_refinement = 20
fem.init_t()

fem.u0 = lambda x, y: 0 * x
fem.v0 = lambda x, y: 0 * x

fem.init_mesh(0.3)
fem.plot_mesh("Circular Mesh")
