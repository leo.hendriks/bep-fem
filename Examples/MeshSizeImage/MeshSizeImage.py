from FEM import FEM
import numpy as np
import matplotlib.pyplot as plt
import os

from scipy.integrate import quad
from scipy.special import j0, j1, jn_zeros

def calc_errors():
    c0 = 1500.
    r0 = 1000.
    beta = 10.
    t_end = 1e-3
    sig = 0.1
    p0 = 1.E6
    f0 = 1.E5
    td = 6. / f0
    tw = 3. / f0
    tend = 2 * td


    # Parameters
    Radius = 1.5 * c0 * t_end
    print(f"R equals: {Radius}")




    def p_source(t):
        return p0 * np.sin(2 * np.pi * f0 * (t - td)) * np.exp(- ((t - td) / (tw / 2)) ** 2)

    x_shock = r0 * c0 ** 3 / (beta * p0 * 2 * np.pi * f0)
    Lx = x_shock + tend * c0
    Ly = Lx / 100.


    def ini(r, A, sigma):
        return A * np.exp(- r ** 2 / 2 / sigma ** 2)


    def integrand(r, zero, R, A, sigma):
        return ini(r, A, sigma) * j0(zero / R * r) * r


    def calc_cn(n, jzeros, R, A, sigma):
        result = np.zeros(n)
        for i in range(n):
            result[i] = 2 * quad(integrand, 0, R, args=(jzeros[i], R, A, sigma), limit=500)[0] / (
                    R * j1(jzeros[i])) ** 2
        return result

    n = 1000
    jzeros = jn_zeros(0, n)
    cn = calc_cn(n, jzeros, Radius, p0, sig)

    def u(r, t, c0, R):
        r_grid, t_grid, jzeros_grid = np.meshgrid(r, t, jzeros)

        return np.sum(cn * j0(jzeros_grid / R * r_grid) * np.cos(c0 * jzeros_grid / R * t_grid), axis=2)

    lc_1d = c0 / (36 * f0)
    lc_2d = sig / 10
    log_list = np.logspace(2, 0, 20)
    errors_1d = np.empty((log_list.size, ))
    errors_2d = np.empty((log_list.size, ))
    for j, log in enumerate(log_list):
        ##########################################
        # 1D FEM
        ##########################################
        fem = FEM([0, Lx, Lx, 0], [0, 0, Ly, Ly], [0, 0, 0, 1], [p_source])
        fem.beta = beta
        fem.lc = log*lc_1d
        print(f"lc is equal to: {fem.lc}")
        fem.r0 = r0
        fem.c0 = c0

        # Time interval
        fem.t_begin = 0
        fem.t_end = 500E-6
        fem.t_refinement = 10
        fem.init_t()


        fem.dt = lc_1d / (4 * c0)
        fem.max_iters = 50
        fem.damping = 0.7
        fem.min_dif = 1e-9 * p0

        fem.u0 = lambda x, y: 0 * x
        fem.v0 = lambda x, y: 0 * x

        fem.sol_func = lambda x, y, t: p_source(t - x / c0)

        errors_1d[j] = fem.solve_wave(True)
        ##########################################
        # 2D circular FEM
        ##########################################

        fem = FEM([Radius, Radius / 2, -Radius / 2, -Radius, -Radius / 2, Radius / 2],
                  [0, np.sqrt(3) * Radius / 2, np.sqrt(3) * Radius / 2, 0, -np.sqrt(3) * Radius / 2,
                   -np.sqrt(3) * Radius / 2])
        # fem = FEM([0, 2, 2, 1, 1, 0], [0, 0, 1, 1, 2, 2])
        fem.beta = beta
        fem.r0 = r0
        fem.c0 = c0

        fem.t_begin = 0
        fem.t_end = t_end
        fem.t_refinement = 20
        fem.init_t()

        fem.lc = log * lc_2d
        print(f"lc equals: {fem.lc}")
        fem.dt = lc_2d / (4 * fem.c0)

        fem.damping = 0.7
        fem.max_iters = 50
        fem.min_dif = 1e-9 * p0

        source_amp = p0
        source_loc_x = 0.
        source_loc_y = 0.
        source_width = sig
        fem.u0 = lambda x, y: source_amp * np.exp(-(
                    np.power(x - source_loc_x, 2) / (2 * source_width ** 2) + np.power(y - source_loc_y, 2) / (
                        2 * source_width * source_width)))
        fem.v0 = lambda x, y: 0 * x

        fem.sol_func = lambda x, y, t: u(np.sqrt(x ** 2 + y ** 2), t, fem.c0, Radius)

        errors_2d[j] = fem.solve_wave(True)

    lc_list_1d = log_list*lc_1d
    lc_list_2d = log_list*lc_2d

    np.savez("errors.npz", errors_1d=errors_1d, errors_2d=errors_2d, lc_list_1d=lc_list_1d, lc_list_2d=lc_list_2d)


if not os.path.exists("errors.npz"):
    calc_errors()
    print("There is no Error file!")

loaded_vars = np.load("errors.npz")
lc_list_1d = loaded_vars['lc_list_1d'][::-1]
errors_1d = loaded_vars['errors_1d'][::-1]
lc_list_2d = loaded_vars["lc_list_2d"][::-1]
errors_2d = loaded_vars["errors_2d"][::-1]

print("log_list_1d:")
print(lc_list_1d)
print("errors_1d:")
print(errors_1d)
print("log_list_2d:")
print(lc_list_2d)
print("errors_2d:")
print(errors_2d)


categories = np.ones(lc_list_1d.shape, dtype=int)
fit_range = range(0, 5)
categories[fit_range] = 0
colormap = np.array(["tab:blue", "tab:red"])

plt.scatter(lc_list_1d, errors_1d, marker='x', c=colormap[categories])

z = np.polyfit(np.log(lc_list_1d[fit_range]), np.log(errors_1d[fit_range]), 1)
p = np.poly1d(z)
x = np.linspace(min(lc_list_1d), max(lc_list_1d), 1000)
plt.loglog(x, np.exp(p(np.log(x))))
print(z)

categories = np.ones(lc_list_1d.shape, dtype=int)
fit_range = range(0, 11)
categories[fit_range] = 0

colormap = np.array(["tab:orange", "tab:red"])

plt.scatter(lc_list_2d, errors_2d, c=colormap[categories])

z = np.polyfit(np.log(lc_list_2d[fit_range]), np.log(errors_2d[fit_range]), 1)
p = np.poly1d(z)
x = np.linspace(min(lc_list_2d), max(lc_list_2d), 1000)
plt.loglog(x, np.exp(p(np.log(x))))
print(z)

# plt.legend([f"{dt}" for dt in dt_list])
plt.title('lc vs error for Time Dependant')
plt.xlabel('lc (m)')
plt.ylabel('error (Pa)')
plt.legend(("1D fit", "2D fit", "1D", "2D"))
# print((np.log(errors[-1])-np.log(errors[0])) / (np.log(lc_list[-1]) - np.log(lc_list[0])))
plt.show()
