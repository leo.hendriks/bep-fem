from FEM import FEM
import numpy as np

from scipy.integrate import quad
from scipy.special import j0, j1, jn_zeros

c0 = 1500.
r0 = 1000.
beta = 10.
t_end = 1e-3
sig = 0.1
p0 = 1e6


# Parameters
Radius = 1.5 * c0 * t_end
print(f"R equals: {Radius}")

fem = FEM([Radius, Radius / 2, -Radius / 2, -Radius, -Radius / 2, Radius / 2],
          [0, np.sqrt(3) * Radius / 2, np.sqrt(3) * Radius / 2, 0, -np.sqrt(3) * Radius / 2, -np.sqrt(3) * Radius / 2])
# fem = FEM([0, 2, 2, 1, 1, 0], [0, 0, 1, 1, 2, 2])
fem.beta = beta
fem.r0 = r0
fem.c0 = c0

fem.t_begin = 0
fem.t_end = t_end
fem.t_refinement = 20
fem.init_t()

fem.lc = sig / 10
print(f"lc equals: {fem.lc}")
fem.dt = fem.lc/(4*fem.c0)

fem.damping = 0.7
fem.max_iters = 50
fem.min_dif = 1e-9*p0

source_amp = p0
source_loc_x = 0.
source_loc_y = 0.
source_width = sig
fem.u0 = lambda x, y: source_amp*np.exp(-(np.power(x-source_loc_x, 2)/(2*source_width**2) + np.power(y-source_loc_y,2)/(2*source_width*source_width)))
fem.v0 = lambda x, y: 0*x



def ini(r, A, sigma):
    return A*np.exp(- r ** 2 / 2 / sigma ** 2)

def integrand(r, zero, R, A, sigma):
    return ini(r, A, sigma) * j0(zero / R * r) * r

def calc_cn(n, jzeros, R, A, sigma):
    result = np.zeros(n)
    for i in range(n):
        result[i] = 2 * quad(integrand, 0, R, args=(jzeros[i], R, A, sigma), limit=500)[0] / (R * j1(jzeros[i])) ** 2
    return result

def u(r, t, n, c0, R):
    jzeros = jn_zeros(0, n)
    cn = calc_cn(n, jzeros, R, source_amp, source_width)
    r_grid, t_grid, jzeros_grid = np.meshgrid(r, t, jzeros)

    return np.sum(cn * j0(jzeros_grid / R * r_grid) * np.cos(c0 * jzeros_grid / R * t_grid), axis=2)

n = 1000

fem.sol_func = lambda x, y, t: u(np.sqrt(x ** 2 + y ** 2), t, n, fem.c0, Radius)

# fem.solve_wave(True)
# fem.save_solution()

fem.load_solution()
fem.n_nodes = fem.coor.shape[0]
fem.calc_error_with_ana_time()

# fem.plot_mesh()
# fem.plot_solution()
# fem.plot_solution_1d_radial(True, lambda r, t: u(r, t, n, fem.c0, Radius))
fem.sub_plot_solution_1d_radial("Linear Wave Solution", [0, 3, 7, 15], lambda r, t: u(r, t, n, fem.c0, Radius))
fem.sub_plot_error_1d_radial("Linear Wave Error", [0, 3, 7, 15])

