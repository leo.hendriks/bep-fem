from FEM import FEM
import numpy as np
import matplotlib.pyplot as plt
import os

from scipy.integrate import quad
from scipy.special import j0, j1, jn_zeros

def calc_errors():
    from FEM import FEM
    import numpy as np

    Lx = 1
    Ly = Lx / 10

    # Parameters


    lc = 0.002
    log_list = np.logspace(0, 2, 20)
    errors = np.empty((log_list.size, ))
    for j, log in enumerate(log_list):
        print(f"{j}/{log_list.size}")
        fem = FEM([0, Lx, Lx, 0], [0, 0, Ly, Ly], [0, 1, 0, 1], [3 / 2., 6.])

        fem.lc = log * lc
        fem.damping = 0.7
        fem.max_iters = 50
        fem.min_dif = 1e-9
        fem.alpha = -1
        fem.t = range(fem.max_iters)

        fem.sol_func = lambda x, y, t: 6 / (1 + x) ** 2

        # fem.plot_sol_func()

        errors[j] = fem.solve_non_lin_diff()

        print(f"iterations: {fem.u.shape[0]}")

    lc_list = log_list*lc

    np.savez("errors.npz", errors=errors, lc_list=lc_list)


if not os.path.exists("errors.npz"):
    calc_errors()
    print("There is no Error file!")

loaded_vars = np.load("errors.npz")
errors = loaded_vars['errors']
lc_list = loaded_vars['lc_list']

print("lc_list:")
print(lc_list)
print("errors:")
print(errors)

fit_range = range(0, 20)

plt.scatter(lc_list, errors, marker='x')

z = np.polyfit(np.log(lc_list[fit_range]), np.log(errors[fit_range]), 1)
p = np.poly1d(z)
x = np.linspace(min(lc_list), max(lc_list), 1000)
plt.loglog(x, np.exp(p(np.log(x))))
print(z)

plt.title('lc vs error for Non-lin. Diffusion')
plt.xlabel('lc')
plt.ylabel('error')

plt.show()

