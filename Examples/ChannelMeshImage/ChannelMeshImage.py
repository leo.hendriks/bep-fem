from FEM import FEM
import numpy as np
import matplotlib.pyplot as plt

# Parameters
r0 = 1000.
c0 = 1500.
beta = 10.
p0 = 1.E6
f0 = 1.E5
td = 6. / f0
tw = 3. / f0
tend = 2 * td


def p_source(t):
    return p0 * np.sin(2 * np.pi * f0 * (t - td)) * np.exp(- ((t - td) / (tw / 2)) ** 2)

# Plot of p_source
ttemp = np.linspace(0, tend, 1000)

x_shock = r0 * c0 ** 3 / (beta * p0 * 2 * np.pi * f0)
print(f"X-shock equals: {x_shock}")
Lx = x_shock + tend * c0
print(f"Lx equals: {Lx}")
Ly = Lx / 10.
print(f"Ly equals: {Ly}")

# FEM
fem = FEM([0, Lx, Lx, 0], [0, 0, Ly, Ly], [0, 0, 0, 1], [p_source])
fem.beta = beta
print(f"Beta is equal to: {fem.beta}")
fem.r0 = r0
fem.c0 = c0

# Time interval
fem.t_begin = 0
fem.t_end = 500E-6
fem.t_refinement = 10
fem.init_t()

fem.lc = c0 / (18 * f0)
fem.dt = fem.lc / (4 * c0)
fem.max_iters = 20
fem.damping = 1.

fem.u0 = lambda x, y: 0 * x
fem.v0 = lambda x, y: 0 * x

fem.init_mesh(0.03)
fem.plot_mesh("Channel Mesh")
