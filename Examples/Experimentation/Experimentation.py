import numpy as np
from FEM import FEM

c0 = 1500.
r0 = 1000.
beta = 10.
t_end = 1e-3
sig = 0.1
p0 = 1.E6
f0 = 1.E5
td = 6. / f0
tw = 3. / f0
tend = 2 * td

def p_source(t):
    return p0 * np.sin(2 * np.pi * f0 * (t - td)) * np.exp(- ((t - td) / (tw / 2)) ** 2)


x_shock = r0 * c0 ** 3 / (beta * p0 * 2 * np.pi * f0)
Lx = x_shock + tend * c0
Ly = Lx / 100.

##########################################
# 1D FEM
##########################################
fem1d = FEM([0, Lx, Lx, 0], [0, 0, Ly, Ly], [0, 0, 0, 1], [p_source])
fem1d.beta = beta
fem1d.r0 = r0
fem1d.c0 = c0

# Time interval
fem1d.t_begin = 0
fem1d.t_end = 500E-6
fem1d.t_refinement = 10
fem1d.init_t()


fem1d.max_iters = 50
fem1d.damping = 0.7
fem1d.min_dif = 1e-9 * p0

fem1d.u0 = lambda x, y: 0 * x
fem1d.v0 = lambda x, y: 0 * x

fem1d.sol_func = lambda x, y, t: p_source(t - x / c0)

lc_1d = c0 / (36 * f0)

fem1d.lc = lc_1d
fem1d.init_mesh(fem1d.lc)
if callable(fem1d.u0):
    fem1d.u0 = fem1d.u0(fem1d.coor[:, 0], fem1d.coor[:, 1])
if callable(fem1d.v0):
    fem1d.v0 = fem1d.v0(fem1d.coor[:, 0], fem1d.coor[:, 1])

fem1d.create_stiffness_matrix()

fem1d.create_mass_matrix()

fem1d.dt = 10 * lc_1d / (4 * c0)
print(f"dt is equal to: {fem1d.dt}")

fem1d.verlet_integrate_wave(True)

print(f"error equals: {np.max(np.abs(fem1d.u[-1, :] - fem1d.sol_func(fem1d.coor[:, 0], fem1d.coor[:, 1], fem1d.t[-1])))}")