from FEM import FEM
import numpy as np
import matplotlib.pyplot as plt
import os

from scipy.integrate import quad
from scipy.special import j0, j1, jn_zeros

def calc_errors():
    c0 = 1500.
    r0 = 1000.
    beta = 10.
    t_end = 1e-3
    sig = 0.1
    p0 = 1.E6
    f0 = 1.E5
    td = 6. / f0
    tw = 3. / f0
    tend = 2 * td


    # Parameters
    Radius = 1.5 * c0 * t_end
    print(f"R equals: {Radius}")




    def p_source(t):
        return p0 * np.sin(2 * np.pi * f0 * (t - td)) * np.exp(- ((t - td) / (tw / 2)) ** 2)

    x_shock = r0 * c0 ** 3 / (beta * p0 * 2 * np.pi * f0)
    Lx = x_shock + tend * c0
    Ly = Lx / 100.


    def ini(r, A, sigma):
        return A * np.exp(- r ** 2 / 2 / sigma ** 2)


    def integrand(r, zero, R, A, sigma):
        return ini(r, A, sigma) * j0(zero / R * r) * r


    def calc_cn(n, jzeros, R, A, sigma):
        result = np.zeros(n)
        for i in range(n):
            result[i] = 2 * quad(integrand, 0, R, args=(jzeros[i], R, A, sigma), limit=500)[0] / (
                    R * j1(jzeros[i])) ** 2
        return result

    n = 1000
    jzeros = jn_zeros(0, n)
    cn = calc_cn(n, jzeros, Radius, p0, sig)

    def u(r, t, c0, R):
        r_grid, t_grid, jzeros_grid = np.meshgrid(r, t, jzeros)

        return np.sum(cn * j0(jzeros_grid / R * r_grid) * np.cos(c0 * jzeros_grid / R * t_grid), axis=2)

    ##########################################
    # 1D FEM
    ##########################################
    fem1d = FEM([0, Lx, Lx, 0], [0, 0, Ly, Ly], [0, 0, 0, 1], [p_source])
    fem1d.beta = beta
    fem1d.r0 = r0
    fem1d.c0 = c0

    # Time interval
    fem1d.t_begin = 0
    fem1d.t_end = 500E-6
    fem1d.t_refinement = 10
    fem1d.init_t()


    fem1d.max_iters = 50
    fem1d.damping = 0.7
    fem1d.min_dif = 1e-9 * p0

    fem1d.u0 = lambda x, y: 0 * x
    fem1d.v0 = lambda x, y: 0 * x

    fem1d.sol_func = lambda x, y, t: p_source(t - x / c0)

    ##########################################
    # 2D circular FEM
    ##########################################

    fem2d = FEM([Radius, Radius / 2, -Radius / 2, -Radius, -Radius / 2, Radius / 2],
              [0, np.sqrt(3) * Radius / 2, np.sqrt(3) * Radius / 2, 0, -np.sqrt(3) * Radius / 2,
               -np.sqrt(3) * Radius / 2])
    # fem = FEM([0, 2, 2, 1, 1, 0], [0, 0, 1, 1, 2, 2])
    fem2d.beta = beta
    fem2d.r0 = r0
    fem2d.c0 = c0

    fem2d.t_begin = 0
    fem2d.t_end = t_end
    fem2d.t_refinement = 20
    fem2d.init_t()

    fem2d.damping = 0.7
    fem2d.max_iters = 50
    fem2d.min_dif = 1e-9 * p0

    source_amp = p0
    source_loc_x = 0.
    source_loc_y = 0.
    source_width = sig
    fem2d.u0 = lambda x, y: source_amp * np.exp(-(
            np.power(x - source_loc_x, 2) / (2 * source_width ** 2) + np.power(y - source_loc_y, 2) / (
            2 * source_width * source_width)))
    fem2d.v0 = lambda x, y: 0 * x

    fem2d.sol_func = lambda x, y, t: u(np.sqrt(x ** 2 + y ** 2), t, fem2d.c0, Radius)


    lc_1d = c0 / (36 * f0)
    lc_2d = sig / 10
    log_list = np.logspace(1, -1, 20)
    errors_1d = np.empty((log_list.size, ))
    errors_2d = np.empty((log_list.size, ))

    fem1d.lc = lc_1d
    fem1d.init_mesh(fem1d.lc)
    if callable(fem1d.u0):
        fem1d.u0 = fem1d.u0(fem1d.coor[:, 0], fem1d.coor[:, 1])
    if callable(fem1d.v0):
        fem1d.v0 = fem1d.v0(fem1d.coor[:, 0], fem1d.coor[:, 1])

    fem1d.create_stiffness_matrix()

    fem1d.create_mass_matrix()

    fem2d.lc = lc_2d
    fem2d.init_mesh(fem2d.lc)
    if callable(fem2d.u0):
        fem2d.u0 = fem2d.u0(fem2d.coor[:, 0], fem2d.coor[:, 1])
    if callable(fem2d.v0):
        fem2d.v0 = fem2d.v0(fem2d.coor[:, 0], fem2d.coor[:, 1])

    fem2d.create_stiffness_matrix()

    fem2d.create_mass_matrix()


    for j, log in enumerate(log_list):
        fem1d.dt = log * lc_1d / (4 * c0)
        print(f"dt is equal to: {fem1d.dt}")

        fem1d.verlet_integrate_wave(True)

        errors_1d[j] = np.max(np.abs(fem1d.u[-1, :] - fem1d.sol_func(fem1d.coor[:, 0], fem1d.coor[:, 1], fem1d.t[-1])))


        fem2d.dt = log * lc_2d / (4 * fem2d.c0)
        print(f"dt is equal to: {fem2d.dt}")

        fem2d.verlet_integrate_wave(True)

        errors_2d[j] = np.max(np.abs(fem2d.u[-1, :] - fem2d.sol_func(fem2d.coor[:, 0], fem2d.coor[:, 1], fem2d.t[-1])))

    dt_list_1d = log_list*lc_1d / (4 * c0)
    dt_list_2d = log_list*lc_2d / (4 * fem2d.c0)

    np.savez("errors.npz", errors_1d=errors_1d, errors_2d=errors_2d, dt_list_1d=dt_list_1d, dt_list_2d=dt_list_2d)


if not os.path.exists("errors.npz"):
    calc_errors()
    print("There is no Error file!")

loaded_vars = np.load("errors.npz")
dt_list_1d = loaded_vars['dt_list_1d']
errors_1d = loaded_vars['errors_1d']
dt_list_2d = loaded_vars["dt_list_2d"]
errors_2d = loaded_vars["errors_2d"]

errors_1d = np.nan_to_num(errors_1d, nan=1e250)
errors_2d = np.nan_to_num(errors_2d, nan=1e250)

plt.loglog(dt_list_1d, errors_1d, 'x')
plt.loglog(dt_list_2d, errors_2d, 'x')
plt.title('Time Step vs Error')
plt.xlabel('dt (s)')
plt.ylabel('error (Pa)')
plt.legend(("1D Channel", "2D Circular"))
plt.show()

fig = plt.subplot(1, 2, 1)

plot1_indices = range(-11, 0)
dt_list_1d = dt_list_1d[plot1_indices]
errors_1d = errors_1d[plot1_indices]
print("log_list_1d:")
print(dt_list_1d)
print("errors_1d:")
print(errors_1d)

plt.loglog(dt_list_1d, errors_1d, 'x')
plt.suptitle("Time Step vs Error")
plt.title('1D Channel solution')
plt.xlabel('dt (s)')
plt.ylabel('error (Pa)')

plt.subplot(1, 2, 2)

plot2_indices = range(-12, 0)
dt_list_2d = dt_list_2d[plot2_indices]
errors_2d = errors_2d[plot2_indices]
print("log_list_2d:")
print(dt_list_2d)
print("errors_2d:")
print(errors_2d)

plt.loglog(dt_list_2d, errors_2d, 'x')
# plt.legend([f"{dt}" for dt in dt_list])
plt.title('2D Circular solution')
plt.xlabel('dt (s)')
plt.ylabel('error (Pa)')
# print((np.log(errors[-1])-np.log(errors[0])) / (np.log(lc_list[-1]) - np.log(lc_list[0])))
plt.show()
