import numpy as np
from scipy.sparse.linalg import spsolve
from Matrices import assemble_nonlin_matrix_old
from Vectors import assemble_vector, generate_element_vector


def picard_iterate_old(fem):
    """
    Calculates the solution using Verlet Integration

    :param u0: the initial values for the place
    :param v0: the initial values for the speed
    :param t: the time values for which we want the solution values
    :param dt: the time step to use in the Verlet Integration
    :param i_bnd_dir: the nodes on the boundary that have a Direchlet boundary condition
    :param double_diff: function to numerically approach the second derivative given a sol_vector u and time t
    :return: the solution values for all t
    """
    f = assemble_vector(fem.elems, fem.coor, fem.n_nodes, generate_element_vector, 0, fem.source_func)

    s = -fem.s
    for bnd, value in zip(fem.i_bnd_dir, fem.dir_values):
        s[bnd, :] = 0
        s[:, bnd] = 0
        s[bnd, bnd] = 1
        f[bnd] = value

    c = np.zeros((fem.total_iters + 1, fem.n_nodes))

    c[0, :] = spsolve(s.tocsc(), f)
    # c[0, :] = c[0, :] - np.mean(c[0, :])
    for i in range(fem.total_iters):
        N = assemble_nonlin_matrix_old(fem.elems, fem.coor, fem.n_nodes, c[i, :])

        temp = s + fem.alpha*N
        for bnd, value in zip(fem.i_bnd_dir, fem.dir_values):
            temp[bnd, :] = 0
            temp[:, bnd] = 0
            temp[bnd, bnd] = 1
        cl = spsolve(temp.tocsc(), f)
        # cl = cl - np.mean(cl)
        c[i+1, :] = c[i, :] + fem.damping * (cl - c[i, :])

    return c


def picard_iterate(c0, picard_step, damping, max_iter, min_dif, final_only=True, pbar=None):
    """
    Picard Iterates starting at c0 and using picard_step to increment. Damping is applied and
    iteration will stop either when max_iter is reached or the difference is smaller then min_dif.
    If full is true it will return the solution for all iteration steps, otherwise it will just return the solution.

    :param c0: the initial value
    :param picard_step: the iteration step
    :param damping: the damping to apply to the Picard step
    :param max_iter: the maximum number of iterations
    :param min_dif: the minimum difference to stop iteration
    :param final_only: Whether or not to return just the final solution or the solution for all iteration steps
    :param pbar: Optional progress bar to keep track of stats
    :return: the solution values c for all iteration steps or just the final solution and number of iterations
    """

    c = np.zeros((max_iter + 1, c0.size))
    i = 0

    c[i, :] = c0

    while i < max_iter and (i == 0 or np.linalg.norm(c[i, :] - c[i-1, :], 1) > min_dif):
        cl = picard_step(c[i, :])

        c[i+1, :] = c[i, :] + damping * (cl - c[i, :])

        i += 1
        # print(f"The difference equals: {np.linalg.norm(c[i, :] - c[i - 1, :], 1)}")
        # print(f"The while loop conditional equals: {i < max_iter and (i == 0 or np.linalg.norm(c[i, :] - c[i - 1, :], 1) > min_dif)}")
    if pbar:
        pbar.set_postfix(picard_iterations=i)
    if final_only:
        return c[i, :]
    else:
        return c[:i+1, :]
