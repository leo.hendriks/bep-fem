import numpy as np

import matplotlib.pyplot as plt
from Plotting import matrix_plot, mesh_plot, triangle_plot, triangle_plot_function, plot_radial1d, plot_1d_x,\
    triangle_sub_plot, sub_plot_radial1d, sub_plot_1d_x, plot_1d_x_zoom

from Mesh import init_mesh
from Matrices import assemble_stiffness_matrix, assemble_mass_matrix, assemble_matrix_old, \
    generate_element_mass_matrix, generate_element_stiffness_matrix, calculate_area, \
    assemble_nonlin_matrix_old, generate_nonlin_matrix_constants, assemble_nonlin_matrix
from Vectors import assemble_vector, generate_element_vector, assemble_vector_old
from VerletIntegrate import verlet_integrate_wave, verlet_integrate_westervelt, verlet_integrate
from PicardIterate import picard_iterate

import os
from scipy.sparse.linalg import spsolve
from scipy.sparse import diags

from FuncTimer import func_timer_maker

from tqdm import tqdm
from math import ceil

# Encapsulating FEM class
class FEM:
    def __init__(self, vertices_x, vertices_y, boundary_dir=None, dir_values=None):
        # Time related attributes
        self.t_begin = 0
        self.t_end = 1
        self.t_refinement = 10
        self.t = np.linspace(self.t_begin, self.t_end, self.t_refinement)

        # Space related attributes
        self.vertices_x = np.array(vertices_x, dtype=float)
        self.vertices_y = np.array(vertices_y, dtype=float)
        assert (self.vertices_x.shape == self.vertices_y.shape)
        # Index i contains a 1 if the boundary between vertex i and i + 1 has a Direchlet boundary condition
        # Index i contains a 0 if the boundary between vertex i and i + 1 has a Neumann boundary condition
        # The last index contains the boundary type between the last vertex and the first vertex
        # By default the entire boundary is assumed to be Neumann
        self.boundary_dir = boundary_dir
        if boundary_dir is None:
            self.boundary_dir = np.zeros(self.vertices_x.shape, dtype=int)
        if dir_values is None:
            self.dir_values = np.zeros((np.sum(self.boundary_dir),), dtype=float)
        else:
            self.dir_values = dir_values

        assert (len(self.dir_values) == np.sum(self.boundary_dir))

        # The source function
        self.source_func = lambda x, y, t: 0 * x
        self.source_func_space = lambda x, y, t: 0 * x
        self.source_func_time = lambda t: 1

        # The analytical solution
        self.sol_func = lambda x, y, t: 0 * x

        # The speed of the wave
        self.c0 = 1

        # parameters for non lin diffusion
        self.alpha = 1

        # parameters for picard iteration
        self.damping = 1.
        self.max_iters = 20
        self.min_dif = 1e-9

        # parameters for westervelt
        self.beta = 0.
        self.r0 = 1.

        # Initial values
        self.u0 = None
        self.v0 = None

        # The attributes related to the mesh
        self.lc = 1.
        self.n_nodes = 0
        self.coor = None
        self.elems = None
        self.area = None
        self.i_bnd_dir = []
        self.i_bnd_neu = []

        # The attributes related to FEM
        self.s = None
        self.m = None
        self.calc_f = None
        self.f = None
        self.dt = 0.01

        # The attributes related to the solution of the FEM
        self.u = None
        self.diff = None

    def init_t(self):
        self.t = np.linspace(self.t_begin, self.t_end, self.t_refinement)

    @func_timer_maker("Mesh Generation")
    def init_mesh(self, lc):
        # loaded = False
        self.lc = lc
        # if os.path.exists("mesh.npz"):
        #     loaded_vars = np.load("mesh.npz")
        #     if loaded_vars['lc'] == lc and \
        #             np.array_equal(loaded_vars['vertices_x'], self.vertices_x) and \
        #             np.array_equal(loaded_vars['vertices_y'], self.vertices_y) and \
        #             np.array_equal(loaded_vars['boundary_dir'], self.boundary_dir):
        #         print("Loading Mesh...")
        #         self.n_nodes = loaded_vars['n_nodes']
        #         self.node_x = loaded_vars['node_x']
        #         self.node_y = loaded_vars['node_y']
        #         self.elems = loaded_vars['elems']
        #         self.i_bnd_dir = list(loaded_vars['i_bnd_dir'])
        #         self.i_bnd_neu = list(loaded_vars['i_bnd_neu'])
        #         loaded = True
        # if not loaded:
        print("Creating Mesh...")
        self.n_nodes, self.coor, self.elems, self.i_bnd_dir, self.i_bnd_neu = \
            init_mesh(self.vertices_x, self.vertices_y, self.boundary_dir, lc)

        # np.savez("mesh.npz", lc=lc, vertices_x=self.vertices_x, vertices_y=self.vertices_y,
        #          boundary_dir=self.boundary_dir, n_nodes=self.n_nodes, node_x=self.node_x, node_y=self.node_y,
        #          elems=self.elems, i_bnd_dir=np.array(self.i_bnd_dir), i_bnd_neu=np.array(self.i_bnd_neu))

        print("The Number of Nodes = %5d" % self.n_nodes)

        self.area = calculate_area(self.coor, self.elems)

    @func_timer_maker("Source Plot")
    def plot_source_func(self):

        fig, ax, ani = triangle_plot_function(self.coor, self.elems, self.t, self.source_func)
        plt.title('Source Function')

        return fig, ax, ani

    @func_timer_maker("Analytical Plot")
    def plot_sol_func(self):

        fig, ax, ani = triangle_plot_function(self.coor, self.elems, self.t, self.sol_func, "Solution Plot")
        plt.title('Analytical Solution Plot')

        return fig, ax, ani

    @func_timer_maker("Initial Plot")
    def plot_initial(self):

        fig, ax, ani = triangle_plot_function(self.coor, self.elems, self.t, lambda x, y, t: self.u0(x, y), 'Initial Condition')

        return fig, ax, ani

    @func_timer_maker("Mesh Plot")
    def plot_mesh(self, title="Mesh Plot"):
        fig = mesh_plot(self.coor, self.elems, self.i_bnd_dir, self.i_bnd_neu)
        plt.title(title + f" (lc={self.lc} m)")
        return fig

    @func_timer_maker("Stiffness Matrix Generation")
    def create_stiffness_matrix(self):
        self.s = assemble_stiffness_matrix(self.elems, self.coor, self.n_nodes, self.area)
        return self.s

    @func_timer_maker("Stiffness Matrix Generation Old")
    def create_stiffness_matrix_old(self):
        self.s = assemble_matrix_old(self.elems, self.coor, self.n_nodes,
                                     generate_element_stiffness_matrix).tocsc()
        return self.s

    @func_timer_maker("Stiffness Matrix Plot")
    def plot_stiffness_matrix(self):
        return matrix_plot(self.s)

    @func_timer_maker("Stiffness Matrix Comparison")
    def compare_stiffness_matrices(self):
        s_old = self.create_stiffness_matrix_old()
        s_new = self.create_stiffness_matrix()
        s_dif = s_new - s_old
        print(f"The difference between the stiffness matrices equals: {np.linalg.norm(s_dif.data, 1)}")

    @func_timer_maker("Mass Matrix Generation")
    def create_mass_matrix(self):
        self.m = assemble_mass_matrix(self.elems, self.n_nodes, self.area)
        return self.m

    @func_timer_maker("Mass Matrix Generation Old")
    def create_mass_matrix_old(self):
        self.m = assemble_matrix_old(self.elems, self.coor, self.n_nodes,
                                     generate_element_mass_matrix).tocsc()
        return self.m

    @func_timer_maker("Mass Matrix Plot")
    def plot_mass_matrix(self):
        return matrix_plot(self.m)

    @func_timer_maker("Mass Matrix Comparison")
    def compare_mass_matrices(self):
        m_old = self.create_mass_matrix_old()
        m_new = self.create_mass_matrix()
        m_dif = m_new - m_old
        print(f"The difference between the mass matrices equals: {np.linalg.norm(m_dif.data, 1)}")

    @func_timer_maker("N Matrix Comparison")
    def compare_nonlin_matrices(self):
        if callable(self.u0):
            self.u0 = self.u0(self.coor[:, 0], self.coor[:, 1])

        nonlin_old = func_timer_maker("N Generation Old")(assemble_nonlin_matrix_old)(self.elems, self.coor,
                                                                                      self.n_nodes, self.u0)

        i, j, data = func_timer_maker("N constant Generation")(generate_nonlin_matrix_constants)(self.elems, self.area)
        nonlin_new = func_timer_maker("N Generation")(assemble_nonlin_matrix)(i, j, data, self.elems, self.n_nodes,
                                                                              self.u0)
        nonlin_dif = nonlin_new - nonlin_old
        print(f"The difference between the N matrices equals: {np.linalg.norm(nonlin_dif.data, 1)}")

    def setup_f_vector(self, seperated):
        if seperated:
            f0 = assemble_vector(self.elems, self.coor, self.n_nodes, self.area, 0, self.source_func_space)

            def calc_f(t):
                return f0 * self.source_func_time(t)

        else:
            def calc_f(t):
                return assemble_vector(self.elems, self.coor, self.n_nodes, self.area, t, self.source_func)

        self.calc_f = calc_f

    @func_timer_maker("Vector Comparison")
    def compare_vectors(self):
        f_old = func_timer_maker("Vector Generation Old")(assemble_vector_old)(self.elems, self.coor, self.n_nodes,
                                                                               generate_element_vector, 0,
                                                                               self.source_func)
        f_new = func_timer_maker("Vector Generation")(assemble_vector)(self.elems, self.coor, self.n_nodes,
                                                                       self.area, 0,
                                                                       self.source_func)
        f_dif = f_new - f_old
        print(f"The difference between vectors equals: {np.linalg.norm(f_dif, 1)}")

    def apply_boundary(self, u, t):
        for bnd, value in zip(self.i_bnd_dir, self.dir_values):
            if callable(value):
                u[bnd] = value(t)
            else:
                u[bnd] = value
        return u

    @func_timer_maker("Verlet Integration Old")
    def verlet_integrate_wave_old(self, seperated):
        if seperated:
            f0 = assemble_vector(self.elems, self.coor, self.n_nodes, self.area, 0, self.source_func_space)

            def double_diff(un, t):
                return spsolve(self.m, - self.c0 ** 2 * self.s.dot(un) - self.c0 ** 2 * f0 * self.source_func_time(t))
        else:
            def double_diff(un, t):
                return spsolve(self.m,
                               - self.c0 ** 2 * self.s.dot(un) - self.c0 ** 2 * assemble_vector(self.elems, self.coor,
                                                                                                self.n_nodes, self.area,
                                                                                                t,
                                                                                                self.source_func))
        self.u = verlet_integrate_wave(self.u0, self.v0, self.t, self.dt, self.i_bnd_dir, self.dir_values, double_diff)
        return self.u

    @func_timer_maker("Verlet Integration")
    def verlet_integrate_wave(self, seperated):
        self.setup_f_vector(seperated)

        u0 = self.apply_boundary(self.u0, self.t[0])

        u1 = u0 + self.v0 * self.dt + 0.5 * self.dt * self.dt * spsolve(self.m, - self.c0 ** 2 * self.s.dot(u0) -
                                                                        self.c0 ** 2 * self.calc_f(self.t[0]))
        u1 = self.apply_boundary(u1, self.t[0] + self.dt)

        def verlet_step(un, un1, t_value, dt, constants):
            un2 = 2 * un1 - un + dt * dt * spsolve(self.m,
                                                   - self.c0 ** 2 * self.s.dot(un1)
                                                   - self.c0 ** 2 * self.calc_f(t_value))
            return self.apply_boundary(un2, t_value + dt), constants

        with tqdm(total=int(ceil((self.t[-1] - self.t_begin) / self.dt)) + 1) as pbar:
            self.u = verlet_integrate(u0, u1, self.t, self.t_begin, self.dt, verlet_step, pbar=pbar)
        return self.u

    @func_timer_maker("Verlet Integration Fast")
    def verlet_integrate_wave_diag_m(self, seperated):
        m_inv = diags(1 / self.m.diagonal(), format="csc")
        if seperated:
            f0 = assemble_vector(self.elems, self.coor, self.n_nodes,
                                 self.area, 0, self.source_func_space)

            def double_diff(un, t):
                return m_inv.dot(- self.c0 ** 2 * self.s.dot(un) - self.c0 ** 2 * f0 * self.source_func_time(t))
        else:
            def double_diff(un, t):
                return m_inv.dot(
                    - self.c0 ** 2 * self.s.dot(un) - self.c0 ** 2 * assemble_vector(self.elems, self.coor,
                                                                                     self.n_nodes,
                                                                                     self.area, t,
                                                                                     self.source_func))

        self.u = verlet_integrate_wave(self.u0, self.v0, self.t, self.dt, self.i_bnd_dir, double_diff)
        return self.u

    @func_timer_maker("Solution Plot")
    def plot_solution(self, time=True):
        if time:
            fig, ax, ani = triangle_plot(self.coor, self.elems, self.u, 'Wave Solution', self.t)
        else:
            fig, ax, ani = triangle_plot(self.coor, self.elems, self.u, 'Wave Solution')
        return fig, ax, ani

    @func_timer_maker("Solution SubPlots")
    def sub_plot_solution(self, t_values):
        fig, ax = triangle_sub_plot(self.coor, self.elems, self.u, 'Wave Solution', self.t, t_values)
        return fig, ax

    @func_timer_maker("Solution 1D plot radial")
    def plot_solution_1d_radial(self, time=True, ana_func=None):
        if time:
            fig, ax, ani = plot_radial1d(self.coor, self.u, 'Wave Solution', self.t, ana_func)
        else:
            fig, ax, ani = plot_radial1d(self.coor, self.u, 'Wave Solution')
        return fig, ax, ani

    @func_timer_maker("Solution 1D sub_plot radial")
    def sub_plot_solution_1d_radial(self, title, t_ind, ana_func=None):
        fig, ax = sub_plot_radial1d(self.coor, self.u, title, self.t, t_ind, ana_func)
        return fig, ax

    @func_timer_maker("Error 1D sub_plot radial")
    def sub_plot_error_1d_radial(self, title, t_ind, ana_func=None):
        fig, ax = sub_plot_radial1d(self.coor, self.diff, title, self.t, t_ind, ana_func)
        return fig, ax

    @func_timer_maker("Solution 1D plot x")
    def plot_solution_1d_x(self, time=True, ana_func=None):
        if time:
            fig, ax, ani = plot_1d_x(self.coor, self.u, 'Wave Solution', self.t, ana_func)
        else:
            fig, ax, ani = plot_1d_x(self.coor, self.u, 'Wave Solution')
        return fig, ax, ani

    @func_timer_maker("Solution 1D sub_plot x")
    def sub_plot_solution_1d_x(self, title, t_ind, ana_func=None):
        fig, ax = sub_plot_1d_x(self.coor, self.u, title, self.t, t_ind, ana_func)
        return fig, ax

    @func_timer_maker("Error 1D sub_plot x")
    def sub_plot_error_1d_x(self, title, t_ind, ana_func=None):
        fig, ax = sub_plot_1d_x(self.coor, self.diff, title, self.t, t_ind, ana_func)
        return fig, ax

    @func_timer_maker("Solution 1D zoom x")
    def zoom_plot_solution_1d_x(self, title, t_index, xmin, xmax, ana_func=None):
        fig, ax = plot_1d_x_zoom(self.coor, self.u, title, self.t, t_index, xmin, xmax, ana_func)
        return fig, ax

    @func_timer_maker("Error Calculation")
    def calc_error_with_ana_time(self):
        uana = np.zeros((self.t.size, self.n_nodes))
        for i, t in enumerate(self.t):
            uana[i, :] = self.sol_func(self.coor[:, 0], self.coor[:, 1], t)
        self.diff = self.u - uana
        return np.max(np.abs(self.diff[-1, :]))

    def calc_error_with_ana(self):
        uana = self.sol_func(self.coor[:, 0], self.coor[:, 1], 0)
        self.diff = self.u - uana
        return np.max(np.abs(self.diff[-1, :]))

    @func_timer_maker("Error Plot")
    def plot_difference(self, time=True):
        if time:
            fig, ax, ani = triangle_plot(self.coor, self.elems, self.diff, 'Error with the analytical solution', self.t)
        else:
            fig, ax, ani = triangle_plot(self.coor, self.elems, self.diff, 'Error with the analytical solution')
        ax.set_zlabel("error")
        return fig, ax, ani

    @func_timer_maker("Full Wave Solving")
    def solve_wave(self, seperated):
        self.init_mesh(self.lc)
        if callable(self.u0):
            self.u0 = self.u0(self.coor[:, 0], self.coor[:, 1])
        if callable(self.v0):
            self.v0 = self.v0(self.coor[:, 0], self.coor[:, 1])

        self.create_stiffness_matrix()

        self.create_mass_matrix()

        self.verlet_integrate_wave(seperated)

        return self.calc_error_with_ana_time()

    @func_timer_maker("Picard Iteration")
    def picard_iterate(self):
        f = assemble_vector(self.elems, self.coor, self.n_nodes, self.area, 0, self.source_func)
        s = -self.s

        for bnd, value in zip(self.i_bnd_dir, self.dir_values):
            s[bnd, :] = 0
            s[bnd, bnd] = 1
            f[bnd] = value

        c0 = spsolve(s.tocsc(), f)
        i, j, data = generate_nonlin_matrix_constants(self.elems, self.area)

        def picard_step(c):
            N = assemble_nonlin_matrix(i, j, data, self.elems, self.n_nodes, c)

            temp = s + self.alpha * N
            for bnd, value in zip(self.i_bnd_dir, self.dir_values):
                temp[bnd, :] = 0
                temp[bnd, bnd] = 1
            return spsolve(temp, f)

        self.u = picard_iterate(c0, picard_step, self.damping, self.max_iters, self.min_dif, False)
        return self.u

    @func_timer_maker("Non Linear Differential Solving")
    def solve_non_lin_diff(self):
        self.init_mesh(self.lc)

        self.create_stiffness_matrix()

        self.picard_iterate()

        return self.calc_error_with_ana()

    @func_timer_maker("Westervelt Solving")
    def solve_westervelt(self, seperated):
        self.init_mesh(self.lc)

        if callable(self.u0):
            self.u0 = self.u0(self.coor[:, 0], self.coor[:, 1])
        if callable(self.v0):
            self.v0 = self.v0(self.coor[:, 0], self.coor[:, 1])

        self.create_stiffness_matrix()

        self.create_mass_matrix()

        self.verlet_integrate_westervelt(seperated)

        return self.calc_error_with_ana_time()

    @func_timer_maker("Verlet Integration for Westervelt Old")
    def verlet_integrate_westervelt_old(self, seperated):
        if seperated:
            f0 = assemble_vector(self.elems, self.coor, self.n_nodes,
                                 self.area, 0, self.source_func_space)

            def calc_f(t):
                return f0 * self.source_func_time(t)
        else:
            def calc_f(t):
                return assemble_vector(self.elems, self.coor, self.n_nodes, self.area, t,
                                       self.source_func)
        self.u = verlet_integrate_westervelt(self, calc_f)
        return self.u

    @func_timer_maker("Verlet Integration for Westervelt")
    def verlet_integrate_westervelt(self, seperated):
        self.setup_f_vector(seperated)

        ind_i, ind_j, data = generate_nonlin_matrix_constants(self.elems, self.area)

        u0 = self.apply_boundary(self.u0, self.t[0])

        u1 = u0 + self.v0 * self.dt + 0.5 * self.dt * self.dt * spsolve(self.m, - self.c0 ** 2 * self.s.dot(u0) -
                                                                        self.c0 ** 2 * self.calc_f(self.t[0]))
        u1 = self.apply_boundary(u1, self.t[0] + self.dt)

        non_lin_const = self.beta / self.r0 / self.c0 ** 4

        def initiate_constants():
            constants = {}
            constants["Nn"] = assemble_nonlin_matrix(ind_i, ind_j, data, self.elems, self.n_nodes, u0)
            constants["Nn1"] = assemble_nonlin_matrix(ind_i, ind_j, data, self.elems, self.n_nodes, u1)
            return constants

        with tqdm(total=int(ceil((self.t[-1] - self.t_begin) / self.dt)) + 1) as pbar:
            def verlet_step(un, un1, t_value, dt, constants):
                rhs = (self.dt * self.dt * (self.s.dot(un1) + self.calc_f(t_value)) +
                       (2 * non_lin_const * constants["Nn1"] - 2 / self.c0 ** 2 * self.m).dot(un1) -
                       (non_lin_const * constants["Nn"] - 1 / self.c0 ** 2 * self.m).dot(un))

                un2 = spsolve(non_lin_const * constants["Nn1"] - 1 / self.c0 ** 2 * self.m, rhs)
                un2 = self.apply_boundary(un2, t_value + dt)

                # Calculate the constant rhs of the picard step

                def picard_step(cl):
                    N = assemble_nonlin_matrix(ind_i, ind_j, data, self.elems, self.n_nodes, cl)
                    cl1 = spsolve(non_lin_const * N - 1 / self.c0 ** 2 * self.m, rhs)
                    return self.apply_boundary(cl1, t_value + dt)

                un2 = picard_iterate(un2, picard_step, self.damping, self.max_iters, self.min_dif, True, pbar)

                constants["Nn"] = constants["Nn1"]
                constants["Nn1"] = assemble_nonlin_matrix(ind_i, ind_j, data, self.elems, self.n_nodes, un2)
                return un2, constants
            self.u = verlet_integrate(u0, u1, self.t, self.t_begin, self.dt, verlet_step, initiate_constants, pbar)
        return self.u

    @func_timer_maker("Saving the Solution")
    def save_solution(self):
        np.savez("solution.npz", coor=self.coor, elems=self.elems, t=self.t, u=self.u)

    @func_timer_maker("Loading the Solution")
    def load_solution(self):
        if os.path.exists("solution.npz"):
            loaded_vars = np.load("solution.npz")
            self.coor = loaded_vars['coor']
            self.elems = loaded_vars['elems']
            self.t = loaded_vars["t"]
            self.u = loaded_vars["u"]
        else:
            print("There is no Solution file!")
