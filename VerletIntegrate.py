import numpy as np
from Matrices import assemble_nonlin_matrix, generate_nonlin_matrix_constants
from scipy.sparse.linalg import spsolve
from Plotting import matrix_plot


def verlet_integrate(u0, u1, t, t0, dt, verlet_step, initiate_constants=lambda: {}, pbar=None):

        # Set initial values for Verlet Integration
        un = u0
        un1 = u1

        # Set values to initiate the loop
        u_temp = np.zeros((t.size, u0.size))
        t_value = t0 + dt
        if pbar:
            pbar.update(1)
        i = 0

        # Calculate constant values over Verlet integration
        constants = initiate_constants()

        # If one of the evaluated time values is in between t0 and t0 + dt this is called
        if t_value >= t[i]:
            u_temp[i, :] = un + (t[i] - t_value + dt) * (un1 - un) / dt
            i += 1

        # Iterate until t > t_end
        while i < t.size:
            # First we calculate the value for the next time step
            un2, constants = verlet_step(un, un1, t_value, dt, constants)

            t_value += dt
            if pbar:
                pbar.update(1)

            # Again we do a linear interpolation if the time value is in between two values
            if t_value > t[i]:
                u_temp[i, :] = un1 + (t[i] - t_value + dt) * (un2 - un1) / dt
                i += 1
            un = un1
            un1 = un2
        return u_temp


def verlet_integrate_wave(u0, v0, t, dt, i_bnd_dir, dir_values, double_diff):
    """
    Calculates the solution using Verlet Integration

    :param u0: the initial values for the place
    :param v0: the initial values for the speed
    :param t: the time values for which we want the solution values
    :param dt: the time step to use in the Verlet Integration
    :param i_bnd_dir: the nodes on the boundary that have a Direchlet boundary condition
    :param double_diff: function to numerically approach the second derivative given a sol_vector u and time t
    :return: the solution values for all t
    """

    # Set initial values for Verlet Integration
    un = u0
    for bnd, value in zip(i_bnd_dir, dir_values):
        if callable(value):
            un[bnd] = value(t[0])
        else:
            un[bnd] = value

    un1 = un + v0 * dt + 0.5 * dt * dt * double_diff(un, t[0] + dt)
    for bnd, value in zip(i_bnd_dir, dir_values):
        if callable(value):
            un1[bnd] = value(t[0] + dt)
        else:
            un1[bnd] = value

    # Set values to initiate the loop
    utemp = np.zeros((t.size, u0.size))
    t_value = t[0] + dt
    i = 0

    # If one of the evaluated time values is in between t0 and t0 + dt this is called
    if t_value >= t[i]:
        utemp[i, :] = un + (t[i] - t_value + dt) * (un1 - un) / dt
        i += 1

    # Iterate until t > t_end
    while i < t.size:
        # First we calculate the value for the next time step
        un2 = 2 * un1 - un + dt * dt * double_diff(un1, t_value)

        t_value += dt

        # Enforce Dirichlet boundary
        for bnd, value in zip(i_bnd_dir, dir_values):
            if callable(value):
                un2[bnd] = value(t_value)
            else:
                un2[bnd] = value

        # Again we do a linear interpolation if the time value is in between two values
        if t_value >= t[i]:
            utemp[i, :] = un1 + (t[i] - t_value + dt) * (un2 - un1) / dt
            i += 1
        un = un1
        un1 = un2
    return utemp


def verlet_integrate_westervelt(fem, calc_f):
    """
    Calculates the solution using Verlet Integration

    :param u0: the initial values for the place
    :param v0: the initial values for the speed
    :param t: the time values for which we want the solution values
    :param dt: the time step to use in the Verlet Integration
    :param i_bnd_dir: the nodes on the boundary that have a Direchlet boundary condition
    :param double_diff: function to numerically approach the second derivative given a sol_vector u and time t
    :return: the solution values for all t
    """

    ind_i, ind_j, data = generate_nonlin_matrix_constants(fem.elems, fem.area)

    # Set initial values for Verlet Integration
    un = fem.u0
    for bnd, value in zip(fem.i_bnd_dir, fem.dir_values):
        if callable(value):
            un[bnd] = value(fem.t[0])
        else:
            un[bnd] = value
    Nn = assemble_nonlin_matrix(ind_i, ind_j, data, fem.elems, fem.n_nodes, un)

    un1 = un + fem.v0 * fem.dt + 0.5 * fem.dt * fem.dt * spsolve(fem.m, - fem.c0 ** 2 * fem.s.dot(un) - fem.c0 ** 2 *
                                                                 calc_f(fem.t[0]))
    for bnd, value in zip(fem.i_bnd_dir, fem.dir_values):
        if callable(value):
            un1[bnd] = value(fem.t[0] + fem.dt)
        else:
            un1[bnd] = value
    Nn1 = assemble_nonlin_matrix(ind_i, ind_j, data, fem.elems, fem.n_nodes, un1)

    # Set values to initiate the loop
    utemp = np.zeros((fem.t.size, fem.u0.size,))
    t_value = fem.t[0] + fem.dt
    i = 0

    # If one of the evaluated time values is in between t0 and t0 + dt this is called
    if t_value >= fem.t[i]:
        utemp[i, :] = un + (fem.t[i] - t_value + fem.dt) * (un1 - un) / fem.dt
        i += 1

    non_lin_const = fem.beta / fem.r0 / fem.c0 ** 4

    # Iterate until t > t_end
    while i < fem.t.size:
        # First we calculate the value for the next time step
        un2 = 2 * un1 + spsolve(non_lin_const * Nn1 - 1 / fem.c0 ** 2 * fem.m,
                                - non_lin_const * Nn.dot(un) + 1 / fem.c0 ** 2 * fem.m.dot(un) +
                                fem.dt * fem.dt * (fem.s.dot(un1) + calc_f(t_value)))

        t_value += fem.dt

        # Enforce Dirichlet boundary
        for bnd, value in zip(fem.i_bnd_dir, fem.dir_values):
            if callable(value):
                un2[bnd] = value(t_value)
            else:
                un2[bnd] = value

        # Again we do a linear interpolation if the time value is in between two values
        if t_value >= fem.t[i]:
            utemp[i, :] = un1 + (fem.t[i] - t_value + fem.dt) * (un2 - un1) / fem.dt
            i += 1
        un = un1
        Nn = Nn1
        un1 = un2
        Nn1 = assemble_nonlin_matrix(ind_i, ind_j, data, fem.elems, fem.n_nodes, un2)
    return utemp
